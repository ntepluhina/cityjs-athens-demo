import { createRouter, createWebHistory } from 'vue-router'
import Home from './pages/Home.vue'

const routes = [
  { path: '/', component: Home, name: 'home' },
  {
    path: '/characters/:id',
    name: 'character',
    component: () => import('./pages/Character.vue'),
    props: true,
  },
]

export default createRouter({
  routes,
  history: createWebHistory(),
})
